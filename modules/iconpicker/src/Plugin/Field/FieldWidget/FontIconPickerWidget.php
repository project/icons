<?php

namespace Drupal\icons_iconpicker\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\icons_iconpicker\Element\FontIconPickerElement;

/**
 * Provides a "jQuery fontIconPicker" widget to help search and select icons.
 *
 * @FieldWidget(
 *   id = "font_icon_picker",
 *   label = @Translation("Font Icon Picker"),
 *   field_types = {
 *     "list_icon"
 *   },
 * )
 *
 * @see https://fonticonpicker.github.io/
 * @see FontIconPickerElement
 */
class FontIconPickerWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'icon_picker_theme' => 'grey',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $element['icon_picker_theme'] = [
      '#type' => 'select',
      '#title' => $this->t('Icon Picker Theme'),
      '#default_value' => $this->getSetting('icon_picker_theme'),
      '#required' => TRUE,
      '#options' => array_combine(FontIconPickerElement::PICKER_THEMES, FontIconPickerElement::PICKER_THEMES),
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = [];
    $summary[] = $this->t('Icon Picker Theme: %theme', ['%theme' => $this->getSetting('icon_picker_theme')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $element['value'] = $element + [
      '#type' => 'font_icon_picker',
      '#default_value' => $items[$delta]->value ?? NULL,
      '#icon_picker_theme' => $this->getSetting('icon_picker_theme'),
    ];
    return $element;
  }

}
