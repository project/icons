### Icons Iconpicker

Provides a fonticonpicker element for the Icons module.

Refer to https://fonticonpicker.github.io

## Installation

### Option 1: Composer

1. Ensure composer is set up to install npm-assets to the `libraries` folder:
   - Refer to https://www.drupal.org/docs/develop/using-composer/manage-dependencies#third-party-libraries
2. Run `composer require "npm-asset/fonticonpicker--fonticonpicker:^3.1`

### Option 2: Manual installation

1. Go to https://github.com/fontIconPicker/fontIconPicker/releases and download
`fontIconPicker.zip`.
2. Extract the `dist` folder to `libraries/fonticonpicker--fonticonpicker/dist`.

## Usage

## Widget

1. Set up one or more Icon Sets as per `Configuration` in the main module's README.
2. Add a `List (icon)` field.
3. Set the field widget to `Font Icon Picker`.

## Custom Element

Add the following render array to a form array.
```php
$element['icon_picker'] = [
  '#type' => 'font_icon_picker',
  '#title' => 'Icon Picker',
  '#default_value' => 'icon_set_name:icon-id',
  '#icon_picker_theme' => 'darkgrey',
];
```
