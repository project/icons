/**
 * @file
 * Initialize fontIconPicker.
 */

(function ($, once) {
  "use strict";

  Drupal.behaviors.miconElement = {
    attach: function (context) {
      const settings = drupalSettings.FontIconPicker;
      const $iconPickerIcon = $(once('IconsFontPickerIcon', 'input.icons-font-icon-picker', context));
      $iconPickerIcon.each(function (index, element) {
        $(element).fontIconPicker({
          source: settings.source,
          searchSource: settings.searchSource,
          theme: settings.theme,
        });
      });
    },
  };
})(jQuery, once);
