<?php

namespace Drupal\icons_fontawesome\Plugin\IconLibrary;

use Drupal;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\icons\IconLibraryPluginJsonBase;
use Drupal\Component\Serialization\Json;

/**
 * Defines an icon library plugin to integrate FontAwesome with icons module.
 *
 * @IconLibrary(
 *   id = "fontawesome",
 *   label = @Translation("FontAwesome"),
 *   description = @Translation("Integration with FontAwesome for the icons module."),
 * )
 */
class FontAwesome extends IconLibraryPluginJsonBase {

  /**
    +   * {@inheritdoc}
    +   */
  public function build(array $element, ConfigEntityInterface $entity, $name): array {
    $prefix = $this->configuration['prefix'];
    $style = $this->configuration['icons'][$name]['style'];
    $element['#attributes']['class'][] = $prefix . $style;
    $element['#attributes']['class'][] = $prefix . $name;
    $element['#attached']['library'][] = 'icons_fontawesome/' . $entity->id();
    return $element;
  }

  /**
    +   * {@inheritdoc}
    +   */
  public function defaultConfiguration(): array {
    return [
      'library_path' => '',
      'name' => 'fontawesome',
      'prefix' => 'fa-',
      'suffix' => FALSE,
      'icons' => [],
    ];
  }

  /**
    +   * Validate the existence of the file folder based on the given library path.
    +   *
    +   * @param string $library_path
    +   *   Path to the library folder of the fontawesome files.
    +   *
    +   * @return bool
    +   *   Indicating if the path validates.
    +   */
  public function validateLibraryPath(string $library_path): bool {
    $path = Drupal::service('file_system')->realpath($library_path);
    if ($path) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Validate the icon-families.json file existence in the given library path.
   *
   * @param string $library_path
   *   Path to the library folder of the fontawesome files.
   *
   * @return bool
   *   Indicating if the path validates.
   */
  public function validateLibraryJson(string $library_path): bool {
    $json_uri = $library_path . '/metadata/icon-families.json';
    $path = Drupal::service('file_system')->realpath($json_uri);
    if ($path) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Validate the fontawesome css files existence in the given library path.
   *
   * @param string $library_path
   *   Path to the library folder of the fontawesome files.
   *
   * @return bool
   *   Indicating if the paths validate.
   */
  public function validateLibraryCss(string $library_path): bool {
    $file_system = Drupal::service('file_system');
    $style_uri = $library_path . '/css/fontawesome.min.css';
    if (!$file_system->realpath($style_uri)) {
      return FALSE;
    }
    $solid_uri = $library_path . '/css/solid.min.css';
    if (!$file_system->realpath($solid_uri)) {
      return FALSE;
    }
    $brands_uri = $library_path . '/css/brands.min.css';
    if (!$file_system->realpath($brands_uri)) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getIcons(): array {
    $icons = array_keys($this->configuration['icons']);
    return array_combine($icons, $icons);
  }

  /**
   * Process info from icon-families.json into the configuration settings.
   */
  public function processJson(): void {
    $json_uri = $this->getLibraryRealPath() . '/metadata/icon-families.json';
    $path = Drupal::service('file_system')->realpath($json_uri);

    $icons = [];
    if ($path) {
      $json_string = file_get_contents($path);
      $config = Json::decode($json_string);

      foreach ($config as $icon_name => $icon_info) {
        $icons[$icon_name] = [
          'name' => $icon_name,
          'label' => $icon_info['label'],
          'tags' => $icon_info['search']['terms'],
          'style' => $icon_info['familyStylesByLicense']['free'][0]['style'],
        ];
      }
    }

    $this->configuration['name'] = 'fontawesome';
    $this->configuration['prefix'] = 'fa-';
    $this->configuration['suffix'] = FALSE;
    $this->configuration['icons'] = $icons;
  }
}
