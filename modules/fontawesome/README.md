# Icons FontAwesome

Note: Right now this icon set only supports the free version of Font Awesome
and has only been tested with Font Awesome 6.

## Installation

### Option 1: Composer

1. Ensure composer is set up to install npm-assets to the `libraries` folder:
   - Refer to https://www.drupal.org/docs/develop/using-composer/manage-dependencies#third-party-libraries
2. Run `composer require npm-asset/fortawesome--fontawesome-free:^6.5`

### Option 2: Manual installation

1. Go to https://fontawesome.com/download
2. Click the `Free For Web` button.
3. Extract the folder to a folder within Drupal's public folder.
   - From drupal standards I would recommend the `libraries` folder.

## Create a new Icon Set

1. Go to `/admin/appearance/icon_set`
2. Create a new icon set for the fontawesome plugin.
3. Next step after submitting the form is determining the location of the
   fontawesome folder which contains the `css` and `metadata` folders.
   - This path is relative to Drupal's public folder.
   - If installed via composer, this should be:
      - `libraries/fortawesome--fontawesome-free`
4. Submit and you are done. And you can use the notation below to use the added
   icons in your output through render arrays. Or go to your menu and create a new
   link.

## Usage

```php
$icon = [
  '#type' => 'icon',
  '#icon_set' => 'icon_set_configuration_entity_id',
  '#icon_name' => 'icon_name',
];
```

Or you could use a combination of the configuration entity id with the icon
name like this:

```php
$icon = [
  '#type' => 'icon',
  '#icon_id' => 'icon_set_configuration_entity_id:icon_name',
];
```