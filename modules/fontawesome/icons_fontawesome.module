<?php

/**
 * @file
 * Contains icons_fontawesome.module.
 */

/**
 * Implements hook_library_info_build().
 */
function icons_fontawesome_library_info_build(): array {
  $libraries = [];

  $query = Drupal::entityQuery('icon_set')
    ->accessCheck(FALSE)
    ->condition('plugin', 'fontawesome');

  $ids = $query->execute();

  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager */
  $entityTypeManager = Drupal::service('entity_type.manager');
  $iconSetStorage = $entityTypeManager->getStorage('icon_set');

  /** @var \Drupal\icons\Entity\IconSetInterface[] $icon_sets */
  $icon_sets = $iconSetStorage->loadMultiple($ids);

  // Add a library whose information changes depending on certain conditions.
  foreach ($icon_sets as $icon_set) {
    /** @var \Drupal\icons_fontawesome\Plugin\IconLibrary\FontAwesome $plugin */
    $plugin = $icon_set->getPlugin();
    $libraries[$icon_set->id()] = [
      'css' => [
        'component' => [
          $plugin->getLibraryPublicPath() . '/css/fontawesome.min.css' => ['minified' => TRUE],
        ],
        'theme' => [
          $plugin->getLibraryPublicPath() . '/css/solid.min.css' => ['minified' => TRUE],
          $plugin->getLibraryPublicPath() . '/css/brands.min.css' => ['minified' => TRUE],
        ],
      ],
    ];
  }
  return $libraries;
}
